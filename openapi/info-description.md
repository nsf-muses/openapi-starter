This API definition specifies the endpoints for the MUSES module inputs and outputs and the schemas of the associated data structures.

Conventionally, the `paths` spec of an OpenAPI definition refers to the path in a URL at which an HTTP webserver is listening. That path represents an "endpoint" that also requires a particular operation, such as `GET` or `PUT` or `DELETE`. For example, if you define an endpoint `/api/v1/user` and a `GET` operation on that endpoint, you would typically specify in the OpenAPI document how the response will be a JSON structure with a schema like:

```json
{
    "firstname": "Jane",
    "lastname": "Doe",
    "id": 123
}
```

MUSES modules do not implement a webserver; instead, the `paths` spec represents *file paths* that the module will read/write at runtime. The OpenAPI `requestBody` and `responses` specs are interpreted as an input file and output file specification, respectively.