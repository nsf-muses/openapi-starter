FROM node:lts-alpine

RUN mkdir /opt/openapi-starter
WORKDIR /opt/openapi-starter
COPY ./package-lock.json ./package.json ./
RUN npm install
COPY ./ ./